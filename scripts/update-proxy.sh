#!/bin/bash

threshold=10

push_to_redis(){
  avaible_proxy=`echo -e "llen serp_proxy_list" | redis-cli -a "ForSearchAtlasOnlya@" -h "147.75.70.121"`
  echo "[$(date)] Number of avaible proxy: ${avaible_proxy}"
  if [[ "$avaible_proxy" -lt "$threshold" ]]; then
    echo "[$(date)] Number of avaible proxy is below threshold $threshold"
    curl -X GET https://api.myprivateproxy.net/v1/fetchProxies/plain/full/gcbiac24rthyac4fytlxxmnp78vde2ce > proxies.txt
    proxies=`cat proxies.txt | awk -F: '{print $1}' | paste -sd" "`
    redis_cmd="lpush serp_proxy_list ${proxies}"
    del_old="del serp_proxy_list"
    echo -e ${del_old} | redis-cli -a "ForSearchAtlasOnlya@" -h "147.75.70.121"
    echo -e ${redis_cmd} | redis-cli -a "ForSearchAtlasOnlya@" -h "147.75.70.121"
  fi
}

push_to_redis