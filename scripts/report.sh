#!/bin/bash

redis_host="$1"
redis_pwd="$2"
redis_key="$3"

report(){
  current_ts=`date +%s`
  previous_value=0
  previous_timestamp=0
  if [[ -f previous_value ]]; then
    previous_value=`cat previous_value | awk '{print $1}'`
    previous_timestamp=`cat previous_value | awk '{print $2}'`
  fi
  echo "[$(date --date=@${previous_timestamp})] Previous cardination: ${previous_value}"
  current_set_cardination=`echo -e "zcard $redis_key" | redis-cli -a "$redis_pwd" -h "$redis_host"`
  echo "[$(date)] Current cardination: ${current_set_cardination}"
  data=""
  if [[ "$?" -eq 0 ]]; then
    let sofar=1000000-current_set_cardination
    let processed=previous_value-current_set_cardination
    let duration=current_ts-previous_timestamp
    rate=`echo "$processed/$duration" | bc -l`
    rate=`printf '%.2f' $rate`
    hourly_rate=`echo "${rate}*60*60" | bc -l`
    daily_rate=`echo "${hourly_rate}*24" | bc -l`
    hourly_rate=`printf '%.2f' $hourly_rate`
    daily_rate=`printf '%.2f' $daily_rate`
    echo "$current_set_cardination $current_ts" > previous_value
    data=`cat << EndOfMessage
* Keywords done so far: $sofar
* Rate:
    - Secondly: ${rate}/second
    - Hourly: ${hourly_rate}/hour
    - Daily: ${daily_rate}/day
EndOfMessage`
    data="\`\`\`""$data""\`\`\`"
  fi

  echo "$data"
  if [[ ! -z "$data" ]]; then
    curl -X POST -H 'Content-type: application/json' --data "{\"text\":\"$data\"}" https://hooks.slack.com/services/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
  fi
}


report