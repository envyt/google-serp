
const PROTO_PATH = (process.env.SERP_PROTO_PATH === undefined) ? __dirname + '/proto/serp.proto' : process.env.SERP_PROTO_PATH;
const PROTO_SERVER_ADDR = (process.env.PROTO_SERVER_ADDR === undefined) ? "localhost:50051" : process.env.PROTO_SERVER_ADDR;
const logger = require('./logger');
logger.info(`"proto-path": ${PROTO_PATH}, "proto-server-addr": ${PROTO_SERVER_ADDR}`)

const grpc = require('grpc');
const protoLoader = require('@grpc/proto-loader');
const packageDefinition = protoLoader.loadSync(
    PROTO_PATH,
    {keepCase: true,
     longs: String,
     enums: String,
     defaults: true,
     oneofs: true
    });
const serpProto = grpc.loadPackageDefinition(packageDefinition).serp;
const client = new serpProto.SERPService(PROTO_SERVER_ADDR,
                                       grpc.credentials.createInsecure());

const parseSERP = function (serp, keyword) {
  client.ParseSERP({serp: serp, keyword: keyword}, function(err, response) {
      if (err !== null){
          logger.error(err)
      }else{
          logger.child({keyword:keyword}).info(`ParseSERP returns: ${response.message}`);
      }
  });
}

module.exports = parseSERP