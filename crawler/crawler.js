const puppeteer = require('puppeteer-extra') ;
const StealthPlugin = require('puppeteer-extra-plugin-stealth');
const proxyRequest = require('puppeteer-proxy');
const moment = require('moment');
const grpcParseSERP = require('./grpc_client');
const logger = require('./logger');
const PQueue = require('./priority_queue');
const Entities = require('html-entities').XmlEntities;
const entities = new Entities();
var fs = require('fs');
const Redis = require('ioredis');
puppeteer.use(StealthPlugin());
const RecaptchaPlugin = require('puppeteer-extra-plugin-recaptcha')
puppeteer.use(
  RecaptchaPlugin({
    provider: {
      id: '2captcha',
      token: '007b5a34b5cd7f080461f333978c90c0' // REPLACE THIS WITH YOUR OWN 2CAPTCHA API KEY ⚡
    },
    visualFeedback: false // colorize reCAPTCHAs (violet = detected, green = solved)
  })
)

const blockedResourceTypes = [
    'image',
    'media',
    'font',
    'texttrack',
    'object',
    'beacon',
    'csp_report',
    'imageset',
  ];
  
const skippedResources = [
    'id.google.com',
    // 'adservice.google',
    'googleusercontent.com',
    'doubleclick.net',
    'apis.google.com',
    'ggpht.com',
    'ogs.google.com',
    'play.google.com'
];

const ProxyPool = function ProxyPool(key, redisClient){
    this.disabledIpKey = `${key}_disabled`

    this.getProxy = async function(){
        let ip = "";
        await redisClient.brpoplpush(key, [key, 0]).then(function (result) {
            ip = result;
        });
        return ip;
    }

    this.disableProxy = async function(ip){
        await redisClient.lrem(key, 0, ip);
        await redisClient.hset(this.disabledIpKey, ip, new Date().getTime());
    }
}

const Crawler = function(proxyPool, pqueue, opts){
    this.browser = null;

    this._init = async function(){
        const setup = async () => {
            if (this.browser != null && this.browser.isConnected()){
                await this.browser.close();
            }
            this.browser = await puppeteer.connect({
                browserWSEndpoint: opts.websocketUrl,
                slowMo: 200
            }); 
        }
        await setup();
    }

    this._mkProxy = function(ip){
        return `http://athuso:8L6Yq9Tg@${ip}:29842`
    }

    this._dumpContent = async function(page, response, keyword){
        let kLog = logger.child({ keyword: keyword });
        let fileName = `${keyword.replace("/", "")}_${response._status}_${new Date().getTime()}`
        let html = await page.content();
        let content = entities.decode(html);
        fs.writeFile(`html/${fileName}.html`, content, function (err) {
            if (err) {
                kLog.error("Save html failed");
                throw(err);
            }
            kLog.info('Dump content successfully');
        }); 
    }

    this._grpcParse = async function (page, keyword) {
        let html = await page.content();
        let content = entities.decode(html);
        grpcParseSERP(content, keyword);
    }

    this._isRecaptchaPage = async function (page){
        const iframe = await page.$("iframe");
        if (iframe == null){
            return false
        }
        const srcProp = await iframe.getProperty('src');
        const src = await srcProp.jsonValue();
        if(src.startsWith("https://www.google.com/recaptcha")){
          return true
        } else{
          return false
        }
    }

    this._solveRecaptcha = async function(page, kLog){
        try{
            kLog.info("Encounter recaptcha page")
            await page.solveRecaptchas()
            await Promise.all([
                page.waitForNavigation()
            ])
            kLog.info("Solved recaptcha")
        }catch(err){
            kLog.error(`Solve recaptcha had error: ${err}`);
        } finally{
            await proxyPool.disableProxy(proxyIp);
            kLog.info(`Disable proxy`);
        } 
    }

    this._crawlKeyword = async function(keyword){
        let proxy = ""
        let kLog = logger.child({ keyword: keyword });
        kLog.info("Start processing keyword");
        let page = await this.browser.newPage();
        let url = `https://www.google.com/search?gl=us&q=${keyword}&num=100`;
        let startTime = moment();
        let proxyIp = "";

        if (opts.usePuppeteerProxy){
            proxyIp = await proxyPool.getProxy();
            proxy = this._mkProxy(proxyIp);
            kLog.info(`Using proxy: [${proxyIp}]`);
            kLog = kLog.child({ proxy: proxyIp });
        }

        await page.setRequestInterception(true);

        if (opts.usePuppeteerProxy){
            page.on('request', async (request) => {
                const requestUrl = request._url.split('?')[0].split('#')[0];
                if (request.isNavigationRequest() && request.redirectChain().length && requestUrl.startsWith("https://www.google.com/sorry")){
                    await proxyPool.disableProxy(proxyIp);
                    kLog.child({url:requestUrl}).info(`Encounter recaptcha redirection. Disable proxy`);
                    await request.abort();
                } else if (
                    blockedResourceTypes.indexOf(request.resourceType()) !== -1 ||
                    skippedResources.some(resource => requestUrl.indexOf(resource) !== -1) ||
                    (requestUrl.startsWith("https://www.google.com") && !requestUrl.startsWith("https://www.google.com/search"))
                ) {
                    await request.abort();
                } else {
                    await proxyRequest.proxyRequest({
                        page,
                        proxyUrl: proxy,
                        request,
                    });
                }
            });
        } else if (opts.useHa){
            page.on('request', (request) => {
                const requestUrl = request._url.split('?')[0].split('#')[0];
                if (
                    blockedResourceTypes.indexOf(request.resourceType()) !== -1 ||
                    skippedResources.some(resource => requestUrl.indexOf(resource) !== -1) ||
                    (requestUrl.startsWith("https://www.google.com") && !requestUrl.startsWith("https://www.google.com/search"))
                ) {
                    request.abort();
                } else {
                    if (requestUrl.startsWith("https://www.google.com/search")){
                        const headers = request.headers();
                        headers['sec-fetch-user']='?0';
                        request.continue({headers});
                    }else{
                        request.continue();
                    }
                }
            });
        }

        const response = await page.goto(url, {
            timeout: 60000,
            waitUntil: 'networkidle2',
        });

        // handle response
        kLog.info(`Crawling returns code ${response._status}`);
        if (response._status !== 200){
            if (opts.solveRecaptcha && 
                (response._status === 429 || await this._isRecaptchaPage(page))){
                await this._solveRecaptcha(page, kLog);
            }  else {
                await proxyPool.disableProxy(proxyIp);
                kLog.info(`Disable proxy`);
            } 
        }

        try{
            if(opts.dumpContent){
                await this._dumpContent(page, response, keyword);
            }
            if(opts.grpcParse){
                await this._grpcParse(page, keyword);
            }
        }catch(err){
            throw (err);
        }

        page.close().catch(function(value) {
            logger.error("page close failed");
        });
        let endTime = moment();
        var secondsDiff = endTime.diff(startTime, 'seconds');
        kLog.info(`Took ${secondsDiff}s`);
    }

    this.crawl = async function(){
        await this._init();
        await new Promise(resolve => setTimeout(resolve, 5000));
        logger.info("Start crawling process");
        for(;;){
            try{
                let element = await pqueue.blockingGetOne();
                if (element.element !== ""){
                    try{
                        await this._crawlKeyword(element.element);
                    }catch(err){
                        logger.child({ keyword: element.element }).error(`Crawl process had failed. Error: ${err}`);
                        pqueue.pushOne(element.element, parseInt(element.score) + 1);
                        logger.child({ keyword: element.element, score: element.score }).info(`Push back to queue`);
                        await this._init();
                    }
                }
            } catch(err){
                logger.error(err);
            }
        }
    }
}

module.exports = { Crawler, ProxyPool };