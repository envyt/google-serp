const PQueue = require('./priority_queue');
const {Crawler, ProxyPool} = require('./crawler');
const Redis = require('ioredis');
const logger = require('./logger');

const redisAddr = (process.env.REDIS_ADDRESS === undefined) ? "127.0.0.1" : process.env.REDIS_ADDRESS; 
const redisPort = (process.env.REDIS_PORT === undefined) ? 6370 : parseInt(process.env.REDIS_PORT);
const redisDb = (process.env.REDIS_DB === undefined) ? 0 : parseInt(process.env.REDIS_DB);
const redisPassword = (process.env.REDIS_PASSWORD === undefined) ? "" : process.env.REDIS_PASSWORD;
const redisProxyKey = (process.env.REDIS_PROXY_KEY === undefined) ? "proxy" : process.env.REDIS_PROXY_KEY;
const redisQueueKey = (process.env.REDIS_QUEUE_KEY === undefined) ? "queue" : process.env.REDIS_QUEUE_KEY;
const webSocketUrl = (process.env.WEB_SOCKET_URL === undefined) ? "localhost:3000" : process.env.WEB_SOCKET_URL;
const solveRecaptcha = (process.env.SOLVE_RECAPTCHA === "true");
const dumpContent = (process.env.DUMP_SERP_CONTENT === "true");
const grpcParseSERP = (process.env.GRPC_PARSE_SERP === "true");
const proxyMethod = process.env.PROXY_METHOD;

const redisOpts = {
    host: redisAddr,
    port: redisPort,
    db: redisDb,
    password: redisPassword
};
logger.info(`redisAddr: ${redisAddr}, redisPort: ${redisPort}, redisDb: ${redisDb}, webSocketUrl: ${webSocketUrl}, proxyMethod: ${proxyMethod}`);

const redis = new Redis(redisOpts)
const pool = new ProxyPool(redisProxyKey, redis);
const pqueue = new PQueue(redisQueueKey, redis);

const crawler = new Crawler(pool, pqueue, {
    websocketUrl: `ws://${webSocketUrl}`,
    usePuppeteerProxy: proxyMethod === "puppeteer-proxy",
    useHa: proxyMethod === "haproxy",
    solveRecaptcha: solveRecaptcha,
    dumpContent: dumpContent,
    grpcParse: grpcParseSERP
})

async function crawl(){
    await new Promise(resolve => setTimeout(resolve, 15000));
    await crawler.crawl();
}

crawl().catch(function(value) { console.log(value) })