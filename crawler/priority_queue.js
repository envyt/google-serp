const logger = require('./logger');
const Redis = require('ioredis');


const PQueue = function PQueue(key, redisClient){
    this.tracking_key = `${key}_tracking`

    this.pushOne = function(element, score){
        var pipeline = redisClient.pipeline();
        pipeline.zadd(key, score, element);
        pipeline.hset(this.tracking_key, element, score);
        pipeline.exec(function(err, results){
            for (result of results){
                if (result[0] !== null && typeof result[0] !== "undefined"){
                    logger.error(`Push element [${element}] in queue has error: ${result[0]}`);
                }
            }
        });
    }

    this.blockingGetOne = async function(){
        let element = {element: "", score: ""}
        await redisClient.bzpopmin(key, 0).then(function (result) {
            try{
                element.element = result[1];
                element.score = result[2];
            } catch(err){
                logger.error(`Blocking get element from queue had failed. Error: ${err}`);
            }
        });
        return element;
    }
}

module.exports = PQueue;
