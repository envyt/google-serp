from concurrent import futures
import redis
import grpc
import logging
import traceback
import serp_pb2_grpc, serp_pb2
from GoogleScraper import parsing
from cassandra.query import BatchStatement, ConsistencyLevel
from cassandra.cluster import Cluster
from fnvhash import fnv1a_64
from GoogleScraper.parsing import GoogleParser
import uuid
import json
import os


class SERPServer(serp_pb2_grpc.SERPService):
    redis: redis.Redis

    def __init__(self, rcl, scylla, opts):
        self.opts = opts
        self.redis = rcl
        self.scylla = scylla
        self.logger = logging.getLogger('__serp__')

    def AddKeyword(self, request, context):
        try:
            self.redis.zadd(self.opts.redis_keyword_queue, {request.keyword: request.priority})
            return serp_pb2.Reply(message=serp_pb2.SUCCESS)
        except:
            self.logger.error('Add keyword to queue had failed. Error: %s', traceback.format_exc())
            return serp_pb2.Reply(message=serp_pb2.FAILED)

    def ParseSERP(self, request, context):
        try:
            parser = parsing.get_parser_by_search_engine("google")
            parser = parser({}, html=request.serp)
            parser.parse()
            batch = BatchStatement(consistency_level=ConsistencyLevel.QUORUM)
            keyword_id = fnv1a_64(request.keyword.encode())
            self._insert_keyword(batch, request.keyword, keyword_id)
            if isinstance(parser, GoogleParser) and isinstance(parser.search_results, dict):
                search_results = parser.search_results
                self._insert_ads_main(batch, keyword_id, search_results.get('ads_main', [{}]))
                self._insert_maps_local(batch, keyword_id, search_results.get('maps_local', [{}]))
                self._insert_results(batch, keyword_id, search_results.get('results', [{}]))
            self.scylla.execute(batch)
            return serp_pb2.Reply(message=serp_pb2.SUCCESS)
        except:
            self.logger.error('Parse SERP had failed. Error: %s', traceback.format_exc())
            return serp_pb2.Reply(message=serp_pb2.FAILED)

    def GetSERP(self, request, context):
        try:
            keyword_id = fnv1a_64(request.keyword.encode())
            ads = self._get_ads_main(keyword_id)
            maps = self._get_maps_local(keyword_id)
            results = self._get_results(keyword_id)
            return serp_pb2.KeywordSERPAttribute(ads_main=ads, maps_local=maps, results=results)
        except:
            self.logger.error('Parse SERP had failed. Error: %s', traceback.format_exc())
            return serp_pb2.KeywordSERPAttribute(ads_main=[], maps_local=[], results=[])

    def _get_ads_main(self, keyword_id):
        query = self.scylla.prepare('SELECT rank, link, snippet, visible_link FROM serp_ads_main WHERE keyword_id=?')
        ads = self.scylla.execute(query, [keyword_id])
        att_ads = []
        for ad in ads:
            att_ads.append(serp_pb2.Att_AdsMain(
                rank=ad.rank,
                link=ad.link,
                snippet=ad.snippet,
                visible_link=ad.visible_link
            ))
        return att_ads

    def _get_maps_local(self, keyword_id):
        query = self.scylla.prepare('SELECT rank, rating, link, num_reviews, snippet, title FROM serp_maps_local WHERE keyword_id=?')
        maps = self.scylla.execute(query, [keyword_id])
        att_maps = []
        for m in maps:
            att_maps.append(serp_pb2.Att_MapLocal(
                rank=m.rank,
                rating=m.rating,
                link=m.link,
                num_reviews=m.num_reviews,
                snippet=m.snippet,
                title=m.title
            ))
        return att_maps

    def _get_results(self, keyword_id):
        query = self.scylla.prepare(
            'SELECT rank, link, snippet, title, visible_link FROM serp_results WHERE keyword_id=?')
        results = self.scylla.execute(query, [keyword_id])
        att_results = []
        for re in results:
            att_results.append(serp_pb2.Att_Result(
                rank=re.rank,
                link=re.link,
                snippet=re.snippet,
                title=re.title,
                visible_link=re.visible_link
            ))
        return att_results

    def _insert_keyword(self, batch, keyword, keyword_id):
        query = self.scylla.prepare('INSERT INTO serp_keywords (id, keyword) VALUES (?, ?)')
        batch.add(query, (keyword_id, keyword))

    def _insert_ads_main(self, batch, keyword_id, ads_main):
        query = self.scylla.prepare(
            'INSERT INTO serp_ads_main(id, keyword_id, rank, link, snippet, visible_link) VALUES (?, ?, ?, ?, ?, ?)'
        )
        for ads in ads_main:
            batch.add(query, (
                uuid.uuid4(),
                keyword_id,
                ads.get('rank', 0),
                ads.get('link', None),
                ads.get('snippet', None),
                ads.get('visible_link', None)))

    def _insert_maps_local(self, batch, keyword_id, maps_local):
        query = self.scylla.prepare(
            'INSERT INTO serp_maps_local(id, keyword_id, rank, rating, link, num_reviews, snippet, title) '
            'VALUES (?, ?, ?, ?, ?, ?, ?, ?)'
        )
        for m in maps_local:
            rating = 0.0
            try:
                rating = float(m.get('rating', 0.0))
            except:
                self.logger.error('Can not convert rating to float. Rating: %s', m.get('rating'))

            batch.add(query, (
                uuid.uuid4(),
                keyword_id,
                m.get('rank', 0),
                rating,
                m.get('link', None),
                m.get('num_reviews', None),
                m.get('snippet', None),
                m.get('title', None)))

    def _insert_results(self, batch, keyword_id, results):
        query = self.scylla.prepare(
            'INSERT INTO serp_results(id, keyword_id, rank, link, snippet, title, visible_link) VALUES (?, ?, ?, ?, ?, ?, ?)'
        )
        for re in results:
            batch.add(query, (
                uuid.uuid4(),
                keyword_id,
                re.get('rank', 0),
                re.get('link', None),
                re.get('snippet', None),
                re.get('title', None),
                re.get('visible_link', None)))


class Config:
    redis_host = '127.0.0.1'
    redis_port = 6379
    redis_password = ''
    redis_db = 0
    redis_keyword_queue = "queue"

    scylla_host = ['127.0.0.1']
    scylla_keyspace = 'keyword_serp'

    server_worker = 10
    server_listen_addr = '[::]:50051'

    def from_dict(self, d):
        self.redis_host = d.get('redis_host', '127.0.0.1')
        self.redis_port = d.get('redis_port', 6379)
        self.redis_password = d.get('redis_password', '')
        self.redis_db = d.get('redis_db', 0)
        self.redis_keyword_queue = d.get('redis_keyword_queue', 'queue')

        self.scylla_host = d.get('scylla_host', ['127.0.0.1'])
        self.scylla_keyspace = d.get('scylla_keyspace', 'keyword_serp')

        self.server_worker = d.get('server_worker', 10)
        self.server_listen_addr = d.get('server_listen_addr', '[::]:50051')

    def from_file(self, path):
        try:
            with open(path, 'r') as f:
                data = json.load(f)
                self.from_dict(data)
        except Exception as e:
            logging.error("Load config file [%s] had error: %s", path, e)
            pass


def serve():
    config_path = os.environ.get('SERP_CONFIG_FILE', '')
    if not config_path:
        config_path = './config.json'
    opts = Config()
    opts.from_file(config_path)

    scylla = Cluster(opts.scylla_host, connect_timeout=300, control_connection_timeout=300)
    session = scylla.connect(keyspace=opts.scylla_keyspace)
    rcl = redis.Redis(host=opts.redis_host, port=opts.redis_port, db=opts.redis_db, password=opts.redis_password)
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=opts.server_worker))
    serp_server = SERPServer(rcl=rcl, scylla=session, opts=opts)
    serp_pb2_grpc.add_SERPServiceServicer_to_server(serp_server, server)
    server.add_insecure_port(opts.server_listen_addr)
    server.start()
    server.wait_for_termination()


if __name__ == '__main__':
    logging.basicConfig()
    serve()
